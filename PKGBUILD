# Maintainer: Daniel M. Capella <polyzen@archlinux.org>
# Contributor: Yufan You <ouuansteve at gmail>

_name=vue-language-tools
pkgname=vue-language-server
pkgver=1.8.27
pkgrel=1
pkgdesc='Language server for Vue'
arch=('any')
url=https://github.com/vuejs/language-tools/tree/master/packages/language-server
license=('MIT')
depends=('nodejs')
makedepends=('git' 'npm' 'pnpm')
optdepends=('typescript: for use in typescript.tsdk')
source=("$_name::git+https://github.com/vuejs/language-tools.git#tag=v$pkgver")
b2sums=('64608de0e64be2e77931d9e24463246293303d9c9f440dec217acfd17d604fa613aa2526a5df513247bc91ec64e5bd1d5b37ba91ceb41f0a43cda3788edc3f04')

prepare() {
  cd $_name
  pnpm install --frozen-lockfile
}

build() {
  cd $_name
  pnpm build
}

check() {
  cd $_name
  pnpm test
}

package() {
  local mod_dir=/usr/lib/node_modules/@vue/language-server
  install -d "$pkgdir"/{/usr/bin,$mod_dir}
  ln -s $mod_dir/bin/$pkgname.js "$pkgdir"/usr/bin/$pkgname

  cd $_name/packages/language-server
  # Prune unnecessary packages
  rm -r node_modules
  npm install --production

  cp -r bin node_modules out package.json "$pkgdir"/$mod_dir
  install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE
}
